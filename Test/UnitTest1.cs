﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Lab1;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        Func<double, double> firstIntegral = x => 1 / (1 + Math.Sqrt(x));

        Func<double, double> secondIntegral = x => x * Math.Sin(x) / Math.Pow(Math.Cos(x),2);

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestOddPartition()
        {         
            Calculation c = new Calculation();
            double res = c.SimpsonsRule(0, 4, 1, firstIntegral);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestZeroPartition()
        {
            Calculation с = new Calculation();
            double res = с.SimpsonsRule(0, 4, 0, firstIntegral);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestNegativePartition()
        {
            Calculation с = new Calculation();
            double res = с.SimpsonsRule(0, 4, -10000000, firstIntegral);
        }

        [TestMethod]
        public void TestFirstIntegral()
        {
            Calculation с = new Calculation();
            double res = с.SimpsonsRule(0, 4, 1000000, firstIntegral);

            Assert.AreEqual(res, 1.80277542266378, 0.00000001);
        }

        [TestMethod]
        public void TestSecondIntegral()
        {
            Calculation с = new Calculation();
            double res = с.SimpsonsRule(-1.0471975512, 1.0471975512, 1000000, secondIntegral);

            Assert.AreEqual(res, 1.55487441093676, 0.00000001);
        }

    }
}
