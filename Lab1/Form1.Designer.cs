﻿
namespace Lab1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.upBorder = new System.Windows.Forms.TextBox();
            this.downBorder = new System.Windows.Forms.TextBox();
            this.numPartitions = new System.Windows.Forms.TextBox();
            this.function = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(112, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Верхний предел (до):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 71);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Нижний предел (от):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(109, 137);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Количество разбиений:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(237, 38);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Функция:";
            // 
            // upBorder
            // 
            this.upBorder.Location = new System.Drawing.Point(148, 31);
            this.upBorder.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.upBorder.Name = "upBorder";
            this.upBorder.Size = new System.Drawing.Size(76, 20);
            this.upBorder.TabIndex = 4;
            this.upBorder.Text = "Pi/3";
            // 
            // downBorder
            // 
            this.downBorder.Location = new System.Drawing.Point(148, 86);
            this.downBorder.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.downBorder.Name = "downBorder";
            this.downBorder.Size = new System.Drawing.Size(76, 20);
            this.downBorder.TabIndex = 5;
            this.downBorder.Text = "-Pi/3";
            // 
            // numPartitions
            // 
            this.numPartitions.Location = new System.Drawing.Point(240, 137);
            this.numPartitions.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numPartitions.Name = "numPartitions";
            this.numPartitions.Size = new System.Drawing.Size(76, 20);
            this.numPartitions.TabIndex = 6;
            this.numPartitions.Text = "10000000";
            // 
            // function
            // 
            this.function.Location = new System.Drawing.Point(240, 53);
            this.function.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.function.Name = "function";
            this.function.Size = new System.Drawing.Size(102, 20);
            this.function.TabIndex = 7;
            this.function.Text = "x*sin(x)/cos^2(x)";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(339, 137);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(74, 20);
            this.button1.TabIndex = 8;
            this.button1.Text = "Рассчитать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // result
            // 
            this.result.Location = new System.Drawing.Point(363, 53);
            this.result.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(76, 20);
            this.result.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(360, 38);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Результат:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(346, 56);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "=";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.result);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.function);
            this.Controls.Add(this.numPartitions);
            this.Controls.Add(this.downBorder);
            this.Controls.Add(this.upBorder);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox upBorder;
        private System.Windows.Forms.TextBox downBorder;
        private System.Windows.Forms.TextBox numPartitions;
        private System.Windows.Forms.TextBox function;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox result;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

