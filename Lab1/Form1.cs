﻿using MathNet.Symbolics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private double Integral() 
        {
            Calculation c = new Calculation();
            Func<double, double> downF = SymbolicExpression.Parse(downBorder.Text).Compile("Pi");
            Func<double, double> upF = SymbolicExpression.Parse(upBorder.Text).Compile("Pi");
            double down = downF(Math.PI);
            double up = upF(Math.PI);                
            int n = Convert.ToInt32(numPartitions.Text);
            Func<double, double> f = SymbolicExpression.Parse(function.Text).Compile("x");
            return c.SimpsonsRule(down, up, n, f);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double res = Integral();
            result.Text = Convert.ToString(res);
        }
    }
}
