﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public interface INumericalIntegration
    {
        double SimpsonsRule(double downBorder, double upBorder, int numPartitions, Func<double, double> function);
    }
}
