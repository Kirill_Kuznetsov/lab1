﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Integration;

namespace Lab1
{
    public class Calculation: INumericalIntegration
    {
        public double SimpsonsRule(double downBorder, double upBorder, int numPartitions, Func<double, double> function)
            => SimpsonRule.IntegrateComposite(function, downBorder, upBorder, numPartitions);
    }
}
